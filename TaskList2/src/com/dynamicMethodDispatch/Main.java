package com.dynamicMethodDispatch;

public class Main {

	public static void main(String[] args) {
		A a= new A();
		B b= new B();
		C c= new C();
		
		A ref;
		
		ref=a;
		ref.show();
		
		ref=b;
		ref.show();
		
		ref=c;
		ref.show();
		
	}

}
