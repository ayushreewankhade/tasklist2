package com.collections;

import java.util.PriorityQueue;

public class PriorityQueueDemo {

	public static void main(String[] args) {
		PriorityQueue<Integer> pq = new PriorityQueue<Integer>();
        pq.add(100);
		pq.add(105);
		pq.add(110);
		
		System.out.println(pq.peek()); // printing the top element
	    System.out.println(pq.poll()); // printing the top element and removing it
		System.out.println(pq.peek()); // printing the top element again
	
	}

}
