package com.collections;

import java.util.ArrayDeque;

public class ArrayDequeDemo {

	public static void main(String[] args) {
		ArrayDeque<Integer> dq = new ArrayDeque<Integer>(10);
		  
        // add() method to insert
        dq.add(10);
        dq.add(20);
        dq.add(30);
        dq.add(40);
        dq.add(50);
  
        System.out.println(dq);
 
        dq.clear();
  
        dq.addFirst(70);
        dq.addFirst(80);
 
        dq.addLast(90);
        dq.addLast(100);
  
        System.out.println(dq);

	}

}
