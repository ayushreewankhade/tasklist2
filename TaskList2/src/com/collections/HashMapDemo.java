package com.collections;

import java.util.HashMap;

public class HashMapDemo {

	public static void main(String[] args) {
	HashMap<String, Integer> hm= new HashMap<String, Integer>();
	
	hm.put("Rahul", 101);
	hm.put("Vijay", 102);
	hm.put("Nilesh", 103);
	
	if(hm.containsKey("Vijay")) {
		Integer n= hm.get("Vijay");
		System.out.println(" value of key Vijay is- " +n );
	}
	}
}
