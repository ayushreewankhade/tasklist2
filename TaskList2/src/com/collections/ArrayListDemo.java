package com.collections;

import java.util.ArrayList;

public class ArrayListDemo {

	public static void main(String[] args) {
		ArrayList<Integer> al= new ArrayList<Integer>();
		
		al.add(10);
		al.add(20);
		al.add(50);
		al.add(2,75);
		System.out.println("sizeoflistis="+al.size());
		System.out.println("List="+al);
		System.out.println(al.contains(80));
		
		ArrayList<Integer>al1=new ArrayList<Integer>();
		al1.add(40);
		al1.add(50);
		al1.add(60);
		
		ArrayList<Integer>al2=new ArrayList<Integer>();
		al2.addAll(al);
		al2.addAll(al1);
		System.out.println("Mergelistelementis>>"+al2);
	}

}
