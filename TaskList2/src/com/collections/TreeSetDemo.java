package com.collections;

import java.util.TreeSet;

public class TreeSetDemo {

	public static void main(String[] args) {
		TreeSet<String> ts= new TreeSet<String>();
		
	ts.add("Ram");
	ts.add("Vijay");
	ts.add("Ramesh");
	
	System.out.println(ts);
	System.out.println("first element :" +ts.first());
	System.out.println("last element :" +ts.last());
	
	System.out.println("Greater than Ram: " +ts.higher("Vijay"));
	System.out.println("Smaller than Ram: " +ts.lower("Vijay"));
    
	ts.pollFirst();
	ts.pollLast();
	System.out.println(ts);
	}
}
