package com.collections;

import java.util.Vector;

public class VectorDemo {

	public static void main(String[] args) {
		Vector<Integer> v=new Vector<Integer>();
		System.out.println(v.capacity());
		for(int i=1;i<=10;i++){
		v.addElement(i);
		
	}
		System.out.println(v);
		System.out.println("vectorcapacity="+v.capacity());
		System.out.println("index="+v.get(2));
		v.addElement(11);
		System.out.println("capacity after adding=" +v.capacity());
		System.out.println(v);

	}

}
