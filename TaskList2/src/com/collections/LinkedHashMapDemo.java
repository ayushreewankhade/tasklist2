package com.collections;

import java.util.LinkedHashMap;

public class LinkedHashMapDemo {

	public static void main(String[] args) {
		LinkedHashMap<Integer, String> lhm= new LinkedHashMap<Integer, String>();
		lhm.put(101, "one");
		lhm.put(102, "two");
		lhm.put(103, "three");
		System.out.println(lhm);
		
		System.out.println("check if lhm contains key 103: " +lhm.containsKey(103));
		

	}

}
