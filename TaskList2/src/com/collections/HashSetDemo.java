package com.collections;

import java.util.HashSet;

public class HashSetDemo {

	public static void main(String[] args) {
		HashSet<String> hashSet=new HashSet<String>();
		hashSet.add("ram");
		hashSet.add("shyam");
		hashSet.add(null);
		hashSet.add("vijay");
		hashSet.add("ram");
		System.out.println(hashSet);
		
		hashSet.remove("shyam");
		System.out.println("after removing element " +hashSet);
	
		for( String str : hashSet)
			System.out.print(str + ",");
	}

}
