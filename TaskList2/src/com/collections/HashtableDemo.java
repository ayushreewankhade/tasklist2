package com.collections;

import java.util.Hashtable;
import java.util.Map;

public class HashtableDemo {

	public static void main(String args[]) {
		Hashtable<Integer, String> ht= new Hashtable<>();
		ht.put(1,"A");
		ht.put(2,"B");
		ht.put(3, "C");
		ht.put(4, "E");
		System.out.println("initial map: " +ht);
		ht.put(2, "D");
		System.out.println("updated map: " +ht);
		ht.remove(2);
		System.out.println("after removing: " +ht);
		
		for(Map.Entry<Integer, String> e: ht.entrySet())
			System.out.println(e.getKey()+ " " +e.getValue());

	}
}
