package com.collections;

import java.util.LinkedHashSet;

public class LinkedHastSetDemo {

	public static void main(String[] args) {
		LinkedHashSet<String> lhs = new LinkedHashSet<String>(); 
		lhs.add("Tom");
		lhs.add("Chad");
		lhs.add("Niel");
		lhs.add("Tom");
		lhs.add("Brady");
		
		System.out.println("size of LinkedHashSet :" +lhs.size());
		System.out.println(lhs);
		
		lhs.remove("Chad");
		System.out.println("after removing element " +lhs);
		
		System.out.println("check if LinkedHastSet contains Mike :" +lhs.contains("Mike"));
		
		
	}

}
