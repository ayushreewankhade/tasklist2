package com.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PartitionStream {
  
	public static void main (String args[]) {
		List<Integer> list= Arrays.asList(10, 20, 30, 40, 50);
		Map<Boolean, List<Integer>> result= list.stream().collect(Collectors.partitioningBy(number -> number%20==0));
		
		System.out.println(result);
		
	}
}
