package com.stream;
/*reduce stream to get sum of all elements*/

import java.util.Arrays;
import java.util.List;

public class ReduceStream {
	List<Integer> list= Arrays.asList(10, 20, 30, 40, 50);
	
	int s = list.stream().reduce(0, (num1, num2)-> num1 + num2);
	
	//System.out.println(s);
}
