package com.stream;

import java.util.Arrays;
import java.util.List;

public class SliceStream {

	public static void main(String[] args) {
		List<Integer> list= Arrays.asList(10, 20, 30, 40, 50, 60, 70, 80, 90);
		
		list.stream().skip(2).limit(5).forEach(System.out::println);

	}

}
