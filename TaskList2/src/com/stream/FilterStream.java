package com.stream;

import java.util.Arrays;
import java.util.List;

public class FilterStream {

	public static void main(String[] args) {
		List<Integer> list= Arrays.asList(10, 20, 30, 40, 50);
		
		System.out.println("Stream before filtering: " +list);
		
		System.out.println("Stream after filtering: ");
		
		list.stream().filter(number -> number % 20 == 0).forEach(System.out::println);

	}

}
