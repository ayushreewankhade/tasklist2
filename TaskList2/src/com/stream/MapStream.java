package com.stream;

import java.util.List;
import java.util.Arrays;

public class MapStream {

	public static void main(String[] args) {
		List<Integer> list= Arrays.asList(10, 20, 30, 40, 50);
		
		System.out.println("Stream after applying function:");
		list.stream().map(number -> number + 5).forEach(System.out::println);
	}

}
