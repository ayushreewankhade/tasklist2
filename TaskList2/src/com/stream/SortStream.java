package com.stream;

import java.util.Arrays;
import java.util.List;

public class SortStream {

	public static void main(String[] args) {
		List<Integer> list= Arrays.asList(20, 60, 10, 30, 50, 40);

		list.stream().sorted().forEach(System.out::println);
	}

}
