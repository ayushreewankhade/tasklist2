package com.stream;

import java.util.Arrays;
import java.util.List;

public class GetUniqueElements {

	public static void main(String[] args) {
		List<Integer> list= Arrays.asList(10, 20, 30, 40, 50, 50, 40);
		
		list.stream().distinct().forEach(System.out::println);

	}

}
