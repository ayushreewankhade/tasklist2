package com.stream;

import java.util.Arrays;
import java.util.List;

public class PeekStream {

	public static void main(String args[]) {
		List<Integer> list= Arrays.asList(10, 20, 30, 40, 50);
		
		list.stream().peek(System.out::println).count();
	}
}
